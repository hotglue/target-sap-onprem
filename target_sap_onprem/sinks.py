"""SapOnprem target sink class, which handles writing streams."""


from distutils.command.config import config
from wsgiref import headers
from singer_sdk.sinks import BatchSink
import requests

class SapOnpremSink(BatchSink):
    """SapOnprem target sink class."""

    max_size = 10000  # Max records to write in one batch

    def start_batch(self, context: dict) -> None:
        """Start a batch.
        
        Developers may optionally add additional markers to the `context` dict,
        which is unique to this batch.
        """
        # Sample:
        # ------
        # batch_key = context["batch_id"]
        # context["file_path"] = f"{batch_key}.csv"

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record.

        Developers may optionally read or write additional markers within the
        passed `context` dict from the current batch.
        """
        client = requests.session()
        res = ""
        if self.stream_name=="products":
            res = self.syn_products(record,client)
        elif self.stream_name=="orders":
            res = self.syn_orders(record,client)
        elif self.stream_name=="payments":
            res = self.syn_payments(record,client)
        elif self.stream_name=="fulfilments":
            res = self.syn_fulfilments(record,client)

        
            

    def get_token(self,client,url):
        return client.get(url = url, headers={
            "x-csrf-token":"fetch",
            "content-type":"application/json",
            "Accept":"application/json",
            "connection":"keep-alive"
        },
            auth=(self.config.get("username"), self.config.get("password"))
        )
    def get_headers(self):
        headers = {}
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        return headers
        
    def syn_products(self, record: dict, client):
        url_product = f"{self.config.get('api_url')}ZPRODUCTS_SRV/ProductSet"
        url_csrf = f"{self.config.get('api_url')}ZPRODUCTS_SRV"
        
        product_token = self.get_token(client,url_csrf)
        payload = {
            "ProductNo":record["ProductNo"],
            "Description":record["Description"],
            "ProductType":record["ProductType"],
            "ProductGroup":record["ProductGroup"],
            "BaseUnit":record["BaseUnit"],
            "GrossWeight":record["GrossWeight"]
            
        }
        headers = self.get_headers()
        headers["x-csrf-token"] = product_token.headers["x-csrf-token"]
        
        new_product = client.post(url = url_product, headers=headers, json=payload,auth=(self.config.get("username"), self.config.get("password")), cookies=product_token.cookies)
        res = new_product.text
        return res

    def syn_orders(self, record: dict, client):
        url_orders = f"{self.config.get('api_url')}ZORDERS_SRV/OrderHdrSet"
        url_csrf = f"{self.config.get('api_url')}ZORDERS_SRV"
        
        token = self.get_token(client,url_csrf)
        payload = {
            "Documenttype":record["Documenttype"],
            "SalesOrg":record["SalesOrg"],
            "DistrChannel":record["DistrChannel"],
            "Division":record["Division"],
            "CustomerNumber":record["CustomerNumber"],
            "Currency":record["Currency"],
            "OrderItemSet":record["OrderItemSet"],
            "Response":record["Response"],
            
        }
        headers = self.get_headers()
        headers["x-csrf-token"] = token.headers["x-csrf-token"]
        
        new_record = client.post(url = url_orders, headers=headers, json=payload,auth=(self.config.get("username"), self.config.get("password")), cookies=token.cookies)
        res = new_record.text
        return res

    def syn_payments(self, record: dict, client):
        url_orders = f"{self.config.get('api_url')}ZPAYMENTS_SRV/PaymentHdrSet"
        url_csrf = f"{self.config.get('api_url')}ZORDERS_SRV"
        
        token = self.get_token(client,url_csrf)
        payload = {
            "CompanyCode": self.get_record_data(record,"company",""),
            "PostingDate": self.get_record_data(record,"CreatedOn"),
            "DocumentType": self.get_record_data(record,"DocumentType"),
            "Reference": self.get_record_data(record,"Reference"),
            "Currency": self.get_record_data(record,"Currency"),
            "PaymentItemSet": self.get_record_data(record,"PaymentItemSet",[]),
            
        }
        headers = self.get_headers()
        headers["x-csrf-token"] = token.headers["x-csrf-token"]
        
        new_record = client.post(url = url_orders, headers=headers, json=payload,auth=(self.config.get("username"), self.config.get("password")), cookies=token.cookies)
        res = new_record.text
        return res
    def syn_fulfilments(self, record: dict, client):
        url_orders = f"{self.config.get('api_url')}ZFULFILLMENT_SRV/FulfillmentCreateSet"
        url_csrf = f"{self.config.get('api_url')}ZFULFILLMENT_SRV"
        
        token = self.get_token(client,url_csrf)
        payload = {
            "ShippingPoint": self.get_record_data(record,"ShippingPoint",""),
            "ItemSetCreate": self.get_record_data(record,"ItemSetCreate",[]),
            "Response": self.get_record_data(record,"Response",{}),
            
        }
        
        headers = self.get_headers()
        headers["x-csrf-token"] = token.headers["x-csrf-token"]
        
        new_record = client.post(url = url_orders, headers=headers, json=payload,auth=(self.config.get("username"), self.config.get("password")), cookies=token.cookies)
        res = new_record.text
        return res

    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written."""
        # Sample:
        # ------
        # client.upload(context["file_path"])  # Upload file
        # Path(context["file_path"]).unlink()  # Delete local copy
    def get_record_data(self,record,key,default=""):
        if key in record:
            return record[key]
        else:
            return default        
