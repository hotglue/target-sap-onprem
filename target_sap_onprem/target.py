"""SapOnprem target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_sap_onprem.sinks import (
    SapOnpremSink,
)


class TargetSapOnprem(Target):
    """Sample target for SapOnprem."""

    name = "target-sap-onprem"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "username",
            th.StringType
        ),
        th.Property(
            "password",
            th.StringType
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="http://34.86.68.10:8001/sap/opu/odata/sap/"
        ),
    ).to_dict()
    default_sink_class = SapOnpremSink

if __name__ == '__main__':
    TargetSapOnprem.cli()